/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.c
/// @version 1.
///
/// Print the characteristics of the "int" and "unsigned int" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"

#define ULONG_MIN 0
///////////////////////////////////////////////////////////////////////////////
/// long

/// Print the characteristics of the "long" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "long" datatype
void flowLong() {
    long overflow = LONG_MAX;
   printf("long overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);


    long underflow = LONG_MIN;
   printf("long underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);



}
///////////////////////////////////////////////////////////////////////////////
/// unsigned long

/// Print the characteristics of the "unsigned long" datatype
void doUnsignedLong() {
   printf(TABLE_FORMAT_ULONG, "unsigned long", sizeof(unsigned long)*8, sizeof(unsigned long), ULONG_MIN, ULONG_MAX);
}


/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedLong() {
    unsigned long overflow = ULONG_MAX;
   printf("unsgined long overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);


   unsigned long underflow = ULONG_MIN;
   printf("unsgined long underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);



}

